package com.twu.biblioteca;

import com.twu.biblioteca.controller.BibliotecaApp;
import com.twu.biblioteca.model.*;
import com.twu.biblioteca.view.ConsoleIO;
import com.twu.biblioteca.view.DatabaseDriver;
import com.twu.biblioteca.view.Menu;
import com.twu.biblioteca.view.View;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EntryPoint {
    public static void main(String[] args) throws SQLException {
        View consoleView = new ConsoleIO();
        Menu menu = new Menu(consoleView);
        DatabaseDriver dbDriver = new DatabaseDriver();
        List<Item> libraryItems = new ArrayList<>();
        List<User> usersList = new ArrayList<>();

        libraryItems.addAll(dbDriver.loadBooks());
        libraryItems.addAll(dbDriver.loadMovies());
        usersList.addAll(dbDriver.loadUsers());

        Library library = new Library(libraryItems);
        UserDirectory users = new UserDirectory(usersList);
        BibliotecaApp app = new BibliotecaApp(library, consoleView, users, menu);
        app.startApplication();
    }
}