package com.twu.biblioteca.model;

public class Movie extends Item{
    private final String director;
    private final String year;
    private final Double rating;

    public Movie(int id, String name, String director, String year, Double rating, ItemType type) {
        this.id = id;
        this.name = name;
        this.director = director;
        this.year = year;
        this.type = type;
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "\n" + String.format("%-10s%-25s %-20s %-10s %-10f", id, name, director, year, rating);
    }
}