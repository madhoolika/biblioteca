package com.twu.biblioteca.model;

public class Item {
    int id;
    String name;
    ItemType type;

    Item() {}

    boolean isOfID(int id) {
        return this.id == id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (id != item.id) return false;
        return type == item.type;
    }

    String details() {
        return "\n" + String.format("%-10s%-25s", id, name);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
