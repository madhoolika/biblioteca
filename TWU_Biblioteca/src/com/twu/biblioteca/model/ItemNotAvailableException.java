package com.twu.biblioteca.model;

public class ItemNotAvailableException extends Exception {
    ItemNotAvailableException(String message) {
        super(message);
    }
}
