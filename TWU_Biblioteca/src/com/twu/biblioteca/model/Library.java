package com.twu.biblioteca.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Library {
    private List<Item> availableItems;
    private Map<Item, User> checkedOutItems;

    public Library(List<Item> items) {
        this.availableItems = items;
        this.checkedOutItems = new HashMap<>();
    }

    public String items(ItemType type) {
        StringBuilder builder = new StringBuilder();
        for (Item item : availableItems) {
            if (item.type == type)
                builder.append(item.toString());
        }
        return builder.toString();
    }

    public Item checkOutItem(int id, ItemType type, User user) throws ItemNotAvailableException {
        for (Item item : availableItems) {
            if (item.isOfID(id) && item.type == type) {
                availableItems.remove(item);
                checkedOutItems.put(item, user);
                return item;
            }
        }
        throw new ItemNotAvailableException("Item with id " + id + " is not currently available");
    }

    public boolean returnItem(int id, ItemType type, User user) {
        for (Item item : checkedOutItems.keySet()) {
            User itemUser = checkedOutItems.get(item);
            if ((item.isOfID(id)) && (item.type == type) && (itemUser.equals(user))) {
                checkedOutItems.remove(item);
                availableItems.add(item);
                return true;
            }
        }
        return false;
    }

    public String checkedOutItemDetails(ItemType type) {
        StringBuilder builder = new StringBuilder();
        for (Item item : checkedOutItems.keySet()) {
            if (item.type == type) {
                builder.append(item.details());
                builder.append(String.format("%-15s",checkedOutItems.get(item).name()));
                builder.append(String.format("%-10s", checkedOutItems.get(item).libraryNumber()));
            }
        }
        return builder.toString();
    }
}
