package com.twu.biblioteca.model;

public class Book extends Item {
    private final String author;
    private final String year;

    public Book(int id, String name, String author, String year, ItemType type) {

        this.id = id;
        this.name = name;
        this.author = author;
        this.year = year;
        this.type = type;
    }

    @Override
    public String toString() {
        return "\n" + String.format("%-10s%-25s %-20s %-10s", id, name, author, year);
    }
}