package com.twu.biblioteca.model;

import java.util.List;

public class UserDirectory {
    private List<User> users;

    public UserDirectory(List<User> users) {
        this.users = users;
    }

    private User isRegistered(String libraryNumber) throws UserNotRegisteredException {
        for (User user : users) {
            if (user.hasLibraryNumber(libraryNumber))
                return user;
        }
        throw new UserNotRegisteredException("This user is not registered");
    }

    public User authenticate(String libraryNumber, String password) throws UserNotAuthenticException, UserNotRegisteredException {
        User user;
        try {
            user = isRegistered(libraryNumber);
        } catch (UserNotRegisteredException e) {
            throw new UserNotRegisteredException("This user is not registered");
        }
        if (user.isValid(password)) {
            return user;
        }
        throw new UserNotAuthenticException("These are invalid credentials");
    }
}