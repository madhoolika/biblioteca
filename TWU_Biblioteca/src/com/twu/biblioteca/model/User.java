package com.twu.biblioteca.model;

public class User {
    private String libraryNumber;
    private String password;
    private String name;
    private String emailID;
    private String phoneNumber;
    private String role;

    public User(String libraryNumber, String password, String name, String emailID, String phoneNumber, String role) {
        this.libraryNumber = libraryNumber;
        this.password = password;
        this.name = name;
        this.emailID = emailID;
        this.phoneNumber = phoneNumber;
        this.role = role;
    }

    public static User user(String libraryNumber, String password, String name, String email, String contactNumber, String role) {
        return new User(libraryNumber, password, name, email, contactNumber, role);
    }

    String name() {
        return name;
    }
    String libraryNumber() {
        return libraryNumber;
    }

    public String information() {
        return "Name : " + name + "\n" +
                "Email ID : " + emailID + "\n" +
                "Phone Number : " + phoneNumber + "\n";
    }

    boolean hasLibraryNumber(String libraryNumber) {
        return this.libraryNumber.equals(libraryNumber);
    }

    boolean isValid(String inputPassword) {
        return this.password.equals(inputPassword);
    }

    public boolean isAdmin() {
        return this.role.equalsIgnoreCase("admin");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return libraryNumber != null ? libraryNumber.equals(user.libraryNumber) : user.libraryNumber == null;

    }

    @Override
    public int hashCode() {
        return libraryNumber != null ? libraryNumber.hashCode() : 0;
    }
}
