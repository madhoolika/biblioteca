package com.twu.biblioteca.controller;

import com.twu.biblioteca.model.ItemNotAvailableException;
import com.twu.biblioteca.model.ItemType;
import com.twu.biblioteca.model.Library;
import com.twu.biblioteca.model.User;
import com.twu.biblioteca.view.Menu;
import com.twu.biblioteca.view.View;

public class CheckoutMovie extends MenuAction {
    CheckoutMovie(View view, Library library, User user) {
        this.view = view;
        this.library = library;
        this.user = user;
    }

    @Override
    public void execute() {
        Menu menu = new Menu(view);
        menu.movieCheckOutMenu();
        int id = Integer.parseInt(view.read());
        if (id == 0)
            return;
        try {
            if (library.checkOutItem(id, ItemType.MOVIE, user) != null)
                view.write("Thank you! Enjoy the Movie");
        } catch (ItemNotAvailableException exception) {
            view.write("That movie is not available.");
        }
    }
}
