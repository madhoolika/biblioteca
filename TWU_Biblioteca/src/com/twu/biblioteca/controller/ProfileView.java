package com.twu.biblioteca.controller;

import com.twu.biblioteca.model.User;
import com.twu.biblioteca.view.Header;
import com.twu.biblioteca.view.View;

class ProfileView extends MenuAction {

    ProfileView(View view, User user) {
        this.view = view;
        this.user = user;
    }

    @Override
    public void execute() {
        Header header = new Header(view);
        header.profile();
        view.write(user.information());
    }
}
