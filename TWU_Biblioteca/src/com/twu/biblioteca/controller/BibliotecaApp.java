package com.twu.biblioteca.controller;

import com.twu.biblioteca.model.Library;
import com.twu.biblioteca.model.User;
import com.twu.biblioteca.model.UserDirectory;
import com.twu.biblioteca.view.ConsoleIO;
import com.twu.biblioteca.view.Menu;
import com.twu.biblioteca.view.View;

import java.util.HashMap;

public class BibliotecaApp {
    private final HashMap<Integer, MenuAction> menuActions;
    private View consoleView;
    private Library library;
    private Menu menu;
    private UserDirectory users;

    public BibliotecaApp(Library library, View view, UserDirectory users, Menu menu) {
        this.library = library;
        this.consoleView = view;
        this.users = users;
        this.menu = menu;

        menuActions = new HashMap<>();
        menuActions.put(1, new ListBooks(view, library));
        menuActions.put(2, new ListMovies(view, library));
        menuActions.put(3, new Login(view, library, users));
        menuActions.put(4, new Quit(view));
    }

    public void startApplication() {
        welcome();
        startActions();
    }

    private void welcome() {
        consoleView.write("\t\t\t\tWelcome to Biblioteca!");
    }

    private void startActions() {
        int choice;

        while (true) {
            menu.mainMenu();
            try {
                choice = Integer.parseInt(consoleView.read());
                menuActions.get(choice).execute();
            }
            catch (Exception e) {
                System.out.println("Select a valid option!");
            }
        }
    }
}