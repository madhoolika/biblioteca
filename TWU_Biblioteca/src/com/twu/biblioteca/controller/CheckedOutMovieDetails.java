package com.twu.biblioteca.controller;

import com.twu.biblioteca.model.ItemType;
import com.twu.biblioteca.model.Library;
import com.twu.biblioteca.view.Header;
import com.twu.biblioteca.view.View;

class CheckedOutMovieDetails extends MenuAction {

    CheckedOutMovieDetails(View view, Library library) {
        this.view = view;
        this.library = library;
    }

    @Override
    public void execute() {
        Header header = new Header(view);
        header.checkedOutMovieDetails();
        view.write(library.checkedOutItemDetails(ItemType.MOVIE));
    }
}
