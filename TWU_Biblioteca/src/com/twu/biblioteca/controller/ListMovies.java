package com.twu.biblioteca.controller;

import com.twu.biblioteca.model.ItemType;
import com.twu.biblioteca.model.Library;
import com.twu.biblioteca.view.Header;
import com.twu.biblioteca.view.View;

class ListMovies extends MenuAction {

    ListMovies(View view, Library library) {
        this.view = view;
        this.library = library;
    }

    @Override
    public void execute() {
        Header header = new Header(view);
        header.movie();
        view.write(library.items(ItemType.MOVIE));
    }
}
