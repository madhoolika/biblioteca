package com.twu.biblioteca.controller;

import com.twu.biblioteca.model.*;
import com.twu.biblioteca.view.Menu;
import com.twu.biblioteca.view.View;

import java.util.HashMap;
import java.util.Map;

class Login extends MenuAction {
    private UserDirectory users;
    private Map<Integer, MenuAction> customerMenu;
    private Map<Integer, AuthenticatedMenuAction> authenticatedMenus;
    private Map<Integer, MenuAction> adminMenu;

    Login(View view, Library library, UserDirectory users) {
        this.view = view;
        this.library = library;
        this.users = users;
    }

    private void customerMenuMap() {
        customerMenu = new HashMap<>();
        customerMenu.put(1, new ListBooks(view, library));
        customerMenu.put(2, new ListMovies(view, library));
        customerMenu.put(3, new CheckoutBook(view, library, user));
        customerMenu.put(4, new CheckoutMovie(view, library, user));
        customerMenu.put(5, new ReturnBook(view, library, user));
        customerMenu.put(6, new ReturnMovie(view, library, user));
        customerMenu.put(7, new ProfileView(view, user));
        customerMenu.put(8, new Logout(view));
        customerMenu.put(9, new Quit(view));
    }

    private void adminMenuMap() {
        adminMenu = new HashMap<>();
        adminMenu.put(1, new ListBooks(view, library));
        adminMenu.put(2, new ListMovies(view, library));
        adminMenu.put(3, new CheckedOutBookDetails(view, library));
        adminMenu.put(4, new CheckedOutMovieDetails(view, library));
        adminMenu.put(5, new ProfileView(view, user));
        adminMenu.put(6, new Logout(view));
        adminMenu.put(7, new Quit(view));
    }

    @Override
    public void execute() {
        Menu menu = new Menu(view);
        view.write("Enter Library Number: ");
        String libraryNumber = view.read();
        view.write("Enter Password: ");
        String password = view.read();

        user = getAuthenticatedUser(view, libraryNumber, password);
        if (user == null) return;

        customerMenuMap();
        adminMenuMap();

        if (user.isAdmin()) {
            performAdminAction(view, menu);
        } else {
            performCustomerAction(view, menu);
        }
    }

    private User getAuthenticatedUser(View view, String libraryNumber, String password) {
        try {
            user = users.authenticate(libraryNumber, password);
            view.write("Successful Login!");

        } catch (UserNotAuthenticException | UserNotRegisteredException e) {
            view.write("Invalid Credentials!");
            user = null;
        }
        return user;
    }

    private void performAdminAction(View view, Menu menu) {
        int choice;
        menu.adminMenu();
        choice = Integer.parseInt(view.read());

        while (true) {
            adminMenu.get(choice).execute();

            if (choice == 6) break;

            menu.adminMenu();
            choice = Integer.parseInt(view.read());
        }
    }

    private void performCustomerAction(View view, Menu menu) {
        int choice;
        menu.customerMenu();

        choice = Integer.parseInt(view.read());

        while (true) {
            customerMenu.get(choice).execute();

            if (choice == 8) break;

            menu.customerMenu();
            choice = Integer.parseInt(view.read());
        }
    }
}
