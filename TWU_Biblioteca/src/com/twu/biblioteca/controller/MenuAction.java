package com.twu.biblioteca.controller;

import com.twu.biblioteca.model.*;
import com.twu.biblioteca.view.Header;
import com.twu.biblioteca.view.Menu;
import com.twu.biblioteca.view.View;

public abstract class MenuAction {
    View view;
    Library library;
    User user;

    void execute(){}
}

