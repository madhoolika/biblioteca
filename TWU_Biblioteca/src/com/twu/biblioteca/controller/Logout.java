package com.twu.biblioteca.controller;

import com.twu.biblioteca.view.View;

class Logout extends MenuAction {

    Logout(View view) {
        this.view = view;
    }

    @Override
    public void execute() {
        view.write("Logging Out.. Thank you!");
    }
}
