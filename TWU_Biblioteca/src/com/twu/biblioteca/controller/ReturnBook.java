package com.twu.biblioteca.controller;

import com.twu.biblioteca.model.ItemType;
import com.twu.biblioteca.model.Library;
import com.twu.biblioteca.model.User;
import com.twu.biblioteca.view.Menu;
import com.twu.biblioteca.view.View;

class ReturnBook extends MenuAction {
    ReturnBook(View view, Library library, User user) {
        this.view = view;
        this.library = library;
        this.user = user;
    }
    @Override
    public void execute() {
        Menu menu = new Menu(view);
        menu.bookReturnMenu();

        int id = Integer.parseInt(view.read());
        if (library.returnItem(id, ItemType.BOOK, user))
            view.write("Thank you for returning the book.");
        else
            view.write("That is not a valid book to return.");
    }
}
