package com.twu.biblioteca.controller;

import com.twu.biblioteca.model.ItemNotAvailableException;
import com.twu.biblioteca.model.ItemType;
import com.twu.biblioteca.model.Library;
import com.twu.biblioteca.model.User;
import com.twu.biblioteca.view.Menu;
import com.twu.biblioteca.view.View;

public class CheckoutBook extends MenuAction {

    CheckoutBook(View view, Library library, User user) {
        this.view = view;
        this.library = library;
        this.user = user;
    }

    @Override
    public void execute() {
        Menu menu = new Menu(view);
        menu.bookCheckOutMenu();
        int id = Integer.parseInt(view.read());
        if (id == 0)
            return;
        try {
            if (library.checkOutItem(id, ItemType.BOOK, user) != null)
                view.write("Thank you! Enjoy the book");
        } catch (ItemNotAvailableException exception) {
            view.write("That book is not available.");
        }
    }
}
