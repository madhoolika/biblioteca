package com.twu.biblioteca.controller;

import com.twu.biblioteca.view.View;

class Quit extends MenuAction {

    Quit(View view) {
        this.view = view;
    }

    @Override
    public void execute() {
        view.write("Thank you for using Biblioteca!");
        System.exit(0);
    }
}
