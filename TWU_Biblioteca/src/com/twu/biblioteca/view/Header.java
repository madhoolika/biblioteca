package com.twu.biblioteca.view;

public class Header {

    private View view;

    public Header(View view) {
        this.view = view;
    }

    public void main() {
        String header = "\n--------------------------------------------------------\n" +
                String.format("%30s", "MENU") +
                "\n--------------------------------------------------------\n";
        view.write(header);

    }

    public void customer() {
        String header = "\n--------------------------------------------------------\n" +
                String.format("%30s", "USER MENU") +
                "\n--------------------------------------------------------\n";
        view.write(header);
    }

    public void admin() {
        String header = "\n--------------------------------------------------------\n" +
                String.format("%30s", "ADMIN MENU") +
                "\n--------------------------------------------------------\n";
        view.write(header);
    }

    public void bookCheckOut() {
        String header = "\n--------------------------------------------------------\n" +
                String.format("%33s", "BOOK CHECKOUT") +
                "\n--------------------------------------------------------\n";
        view.write(header);
    }

    public void movieCheckOut() {
        String header = "\n--------------------------------------------------------\n" +
                String.format("%35s", "MOVIE CHECKOUT") +
                "\n--------------------------------------------------------\n";
        view.write(header);
    }

    public void bookReturn() {
        String header = "\n--------------------------------------------------------\n" +
                String.format("%33s", "BOOK RETURN") +
                "\n--------------------------------------------------------\n";
        view.write(header);

    }

    public void movieReturn() {
        String header = "\n--------------------------------------------------------\n" +
                String.format("%33s", "MOVIE RETURN") +
                "\n--------------------------------------------------------\n";
        view.write(header);
    }

    public void profile() {
        String header = "\n--------------------------------------------------------\n" +
                String.format("%33s", "PROFILE") +
                "\n--------------------------------------------------------\n";

        view.write(header);
    }

    public void book() {
        String header = "\n" + String.format("%35s", "BOOKS\n") +
                "\n---------------------------------------------------------------------------\n" +
                String.format("%-10s%-25s %-17s %-10s", "ID", "Book", "Author", "Year Of Publication") +
                "\n---------------------------------------------------------------------------";

        view.write(header);
    }

    public void movie() {
        String header = "\n" + String.format("%35s", "MOVIES\n") +
                "\n---------------------------------------------------------------------------\n" +
                String.format("%-10s%-25s %-20s %-8s %-10s", "ID", "Movie", "Director", "Year", "Rating") +
                "\n---------------------------------------------------------------------------";

        view.write(header);
    }

    public void checkedOutBookDetails() {
        String header = "\n" + String.format("%35s", "CHECKED OUT BOOKS\n") +
                "\n---------------------------------------------------------------------------\n" +
                String.format("%-10s%-25s%-15s%-10s", "ID", "Book", "User", "Library Number") +
                "\n---------------------------------------------------------------------------";

        view.write(header);
    }

    public void checkedOutMovieDetails() {
        String header = "\n" + String.format("%35s", "CHECKED OUT MOVIES\n") +
                "\n---------------------------------------------------------------------------\n" +
                String.format("%-10s%-25s", "ID", "Movie") + "User" +
                "\n---------------------------------------------------------------------------";

        view.write(header);
    }
}
