package com.twu.biblioteca.view;

import com.twu.biblioteca.model.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseDriver {
    private Statement statement = null;
    private Connection connection = null;

    public List<Item> loadBooks() throws SQLException {
        statement = createConnection();
        List<Item> libraryItems = new ArrayList<>();
        ResultSet resultSet = statement.executeQuery("select id,name,type,author,year from libraryItems where type ='BOOK'");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String author = resultSet.getString("author");
            String year = resultSet.getString("year");
            libraryItems.add(new Book(id, name, author, year, ItemType.BOOK));
        }
        resultSet.close();
        closeConnection();
        return libraryItems;
    }

    public List<Item> loadMovies() throws SQLException {
        statement = createConnection();
        List<Item> libraryItems = new ArrayList<>();
        ResultSet resultSet = statement.executeQuery("select id,name,director,year,rating from libraryItems where type = 'MOVIE'");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String director = resultSet.getString("director");
            String year = resultSet.getString("year");
            double rating = resultSet.getDouble("rating");
            libraryItems.add(new Movie(id, resultSet.getString("name"), director, year, rating, ItemType.MOVIE));
        }
        resultSet.close();
        closeConnection();
        return libraryItems;
    }

    public List<User> loadUsers() throws SQLException {
        statement = createConnection();
        List<User> usersList = new ArrayList<>();
        ResultSet resultSet = statement.executeQuery("select libraryNumber, password, name, email, phoneNumber, role from users");
        while (resultSet.next()) {
            String libraryNumber = resultSet.getString("libraryNumber");
            String password = resultSet.getString("password");
            String name = resultSet.getString("name");
            String email = resultSet.getString("email");
            String phoneNumber = resultSet.getString("phoneNumber");
            String role = resultSet.getString("role");
            usersList.add(User.user(libraryNumber, password, name, email, phoneNumber, role));
        }
        resultSet.close();
        closeConnection();
        return usersList;
    }

    private Statement createConnection() throws SQLException {
        try {
            Class.forName("org.sqlite.JDBC");
            this.connection = DriverManager.getConnection("jdbc:sqlite:/Users/mbulusu/IdeaProjects/biblioteca/TWU_Biblioteca/Biblioteca.db");
            statement = connection.createStatement();

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return statement;
    }

    private void closeConnection() throws SQLException {
        statement.close();
        connection.close();
    }
}
