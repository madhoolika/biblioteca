package com.twu.biblioteca.view;

public interface View {
    void write(String s);
    String  read();
}
