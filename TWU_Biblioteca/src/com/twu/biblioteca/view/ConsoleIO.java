package com.twu.biblioteca.view;

import java.util.Scanner;

public class ConsoleIO implements View {
    @Override
    public void write(String s) {
        System.out.println(s);
    }

    @Override
    public String read() {
        Scanner in = new Scanner(System.in);
        String input;
        do {
            input = in.nextLine();
        }while (input.length() == 0);
        return input;
    }
}
