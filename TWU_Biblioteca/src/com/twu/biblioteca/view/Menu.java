package com.twu.biblioteca.view;

public class Menu {
    private View view;
    private Header header;

    public Menu(View view) {
        this.view = view;
        this.header = new Header(view);
    }

    public void mainMenu() {
        header.main();
        String menuList =
                        "\n1. List The Books" +
                        "\n2. List the Movies" +
                        "\n3. Login" +
                        "\n4. Quit" +
                        "\nEnter your choice:";
        view.write(menuList);
    }

    public void customerMenu() {
        header.customer();
        String menuList =
                        "\n1. List The Books" +
                        "\n2. List the Movies" +
                        "\n3. Check out Book" +
                        "\n4. Check out Movie" +
                        "\n5. Return Book" +
                        "\n6. Return Movie" +
                        "\n7. Profile" +
                        "\n8. Logout" +
                        "\n9. Quit" +
                        "\nEnter your choice:";
        view.write(menuList);
    }

    public void adminMenu() {

        header.admin();
        String menuList =
                        "\n1. List The Books" +
                        "\n2. List the Movies" +
                        "\n3. Checked Out Books Details" +
                        "\n4. Checked Out Movies Details" +
                        "\n5. Profile" +
                        "\n6. Logout" +
                        "\n7. Quit" +
                        "\nEnter your choice:";

        view.write(menuList);
    }

    public void bookCheckOutMenu() {
        header.bookCheckOut();
        String menuList = "\n1. Enter the Book ID for the Book you wish to Check Out" +
                        "\n2. Enter 0 to go back to Menu" +
                        "\nEnter your choice: ";

        view.write(menuList);
    }

    public void movieCheckOutMenu() {
        header.movieCheckOut();
        String menuList = "\n1. Enter the Movie ID for the Movie you wish to Check Out" +
                        "\n2. Enter 0 to go back to Menu" +
                        "\nEnter your choice: ";

        view.write(menuList);
    }

    public void bookReturnMenu() {
        header.bookReturn();
        String menuList = "\n1. Enter the Book ID for the Book you wish to Return " +
                        "\n2. Enter 0 to go back to Menu " +
                        "\nEnter your choice: ";

        view.write(menuList);
    }

    public void movieReturnMenu() {
        header.movieReturn();
        String menuList = "\n1. Enter the Movie ID for the Movie you wish to Return " +
                        "\n2. Enter 0 to go back to Menu " +
                        "\nEnter your choice: ";

        view.write(menuList);
    }
}
