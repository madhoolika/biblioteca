package com.twu.biblioteca.view;

import com.twu.biblioteca.view.ConsoleIO;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ConsoleIOTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Test
    public void shouldWriteAnyStringToConsole() {
        ConsoleIO consoleView = new ConsoleIO();
        System.setOut(new PrintStream(outContent));
        consoleView.write("Hello, World!");
        Assert.assertEquals("Hello, World!\n", outContent.toString());
    }
}
