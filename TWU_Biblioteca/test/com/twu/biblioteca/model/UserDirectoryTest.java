package com.twu.biblioteca.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class UserDirectoryTest {

    private UserDirectory users;

    @Before
    public void setUp() {
        users = getUserDirectory();
    }

    @Test
    public void shouldReturnTrueWhenUserNameAndPasswordMatches() throws UserNotAuthenticException, UserNotRegisteredException {
        User user = User.user("111-1111", "qwerty", "Joey", "joey@friends.com", "9848999999", "customer");
        assertEquals(user, users.authenticate("111-1111", "qwerty"));
    }

    @Test(expected = UserNotAuthenticException.class)
    public void shouldReturnFalseWhenUserExistsAndPasswordDoesNotMatch() throws UserNotAuthenticException, UserNotRegisteredException {
        users.authenticate("111-1111", "asdfg");
    }

    @Test(expected = UserNotRegisteredException.class)
    public void shouldReturnFalseWhenUserDoesNotExist() throws UserNotAuthenticException, UserNotRegisteredException {
        users.authenticate("123-1234", "qwerty");
    }

    private UserDirectory getUserDirectory() {
        User admin = User.user("000-0000", "asdf", "Madhu", "madhu@tw.com", "9491804800", "admin");
        User user1 = User.user("111-1111", "qwerty", "Joey", "joey@friends.com", "9848999999", "customer");
        User user2 = User.user("222-2222", "qwerty", "Chandler", "chandler@friends.com", "9848999999", "customer");
        User user3 = User.user("333-3333", "qwerty", "Monica", "monica@friends.com", "9848999999", "customer");
        return new UserDirectory(new ArrayList<>(Arrays.asList(admin, user1, user2, user3)));
    }
}