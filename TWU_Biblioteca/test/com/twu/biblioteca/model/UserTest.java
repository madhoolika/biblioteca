package com.twu.biblioteca.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {

    User user;

    @Before
    public void setup() {
        user = User.user("123-1234", "qwerty", "Joey", "joey@friends.com", "9848999999", "customer");
    }

    @Test
    public void shouldBeAbleToGetUserInformation() {
        assertEquals("Name : Joey\nEmail ID : joey@friends.com\nPhone Number : 9848999999\n", user.information());
    }

    @Test
    public void shouldBeTrueIfLibraryNumberIsSame() {
        assertTrue(user.hasLibraryNumber("123-1234"));
    }

    @Test
    public void shouldBeFalseIfLibraryNumberIsDifferent() {
        assertFalse(user.hasLibraryNumber("555-5555"));
    }

    @Test
    public void shouldBeTrueForValidPassword() {
        assertTrue(user.isValid("qwerty"));
    }

    @Test
    public void shouldBeFalseForInvalidPassword() {
        assertFalse(user.isValid("asdfgh"));
    }

    @Test
    public void shouldBeTrueWhenUserIsAdmin() {
        user = User.user("123-1234", "qwerty", "Joey", "joey@friends.com", "9848999999", "admin");
        assertTrue(user.isAdmin());
    }

    @Test
    public void shouldBeFalseWhenUserIsCustomer() {
        assertFalse(user.isAdmin());
    }
}
