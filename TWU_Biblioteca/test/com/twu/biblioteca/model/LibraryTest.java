package com.twu.biblioteca.model;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static com.twu.biblioteca.model.ItemType.*;
import static org.junit.Assert.*;

public class LibraryTest {

    private Library library;
    private Item book;
    private User user;

    @Before
    public void setup() {
        library = new Library(loadItems());
        book = new Book(1, "Ignited Minds", "Abdul Kalam", "1995", BOOK);
        user = User.user("123-1234", "qwerty", "Joey", "joey@friends.com", "9848999999", "customer");
    }

    @Test
    public void userShouldBeAbleToCheckOutAvailableItem() throws ItemNotAvailableException {
        assertEquals(book, library.checkOutItem(1, BOOK, user));
    }

    @Test(expected = ItemNotAvailableException.class)
    public void userShouldNotBeAbleToCheckOutUnavailableItem() throws ItemNotAvailableException {
        assertEquals(book, library.checkOutItem(10, BOOK, user));
    }

    @Test
    public void userShouldBeAbleToReturnItemTheyCheckedOut() throws ItemNotAvailableException {
        library.checkOutItem(1, BOOK, user);
        assertTrue(library.returnItem(1, BOOK, user));
    }

    @Test
    public void userShouldNotBeAbleToReturnItemNotCheckedOutByThem() throws ItemNotAvailableException {
        User userFoo = User.user("321-9876", "qwerty", "Joey", "joey@friends.com", "9848999999", "customer");
        library.checkOutItem(1, BOOK, user);
        assertFalse(library.returnItem(1, BOOK, userFoo));
    }

    @Test
    public void userShouldNotBeAbleToReturnItemNotCheckedOutAtAll() throws ItemNotAvailableException {
        assertFalse(library.returnItem(1, BOOK, user));
    }

//    @Test
//    public void librarianShouldGetCheckedOutItemDetails() throws ItemNotAvailableException {
//        library.checkOutItem(1, BOOK, user);
//        library.checkOutItem(2, BOOK, user);
//        String generatedDetails = library.checkedOutItemDetails(BOOK);
//        String expectedDetails = "\n" + String.format("%-10s%-25s", 1, "Ignited Minds") + user.name() + "\n" +
//                String.format("%-10s%-25s", 2, "To Kill a Mocking Bird") + user.name();
//        assertEquals(expectedDetails, generatedDetails);
//    }

    private List<Item> loadItems() {
        Item book1 = new Book(1, "Ignited Minds", "Abdul Kalam", "1995", BOOK);
        Item book2 = new Book(2, "To Kill a Mocking Bird", "Harper Lee", "1955", BOOK);
        Item book3 = new Book(3, "Harry Potter 1", "J K Rowling", "1996", BOOK);
        Item book4 = new Book(4, "Harry Potter 2", "J K Rowling", "1997", BOOK);
        Item book5 = new Book(5, "Harry Potter 3", "J K Rowling", "1999", BOOK);
        Item movie1 = new Movie(1, "Home Alone", "abcdef", "2000", 9.0, MOVIE);
        Item movie2 = new Movie(2, "LOR 1", "asdf", "1998", 8.0, MOVIE);
        Item movie3 = new Movie(3, "LOR 2", "qwerty", "2000", 8.5, MOVIE);

        return new ArrayList<>(Arrays.asList(book1, book2, book3, book4, book5, movie1, movie2, movie3));
    }
}