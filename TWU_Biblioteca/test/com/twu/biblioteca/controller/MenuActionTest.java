package com.twu.biblioteca.controller;

import com.twu.biblioteca.model.*;
import com.twu.biblioteca.view.View;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.twu.biblioteca.model.ItemType.BOOK;
import static com.twu.biblioteca.model.ItemType.MOVIE;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MenuActionTest {

    private Library library;
    private User user;
    private View mockConsoleIO;

    @Before
    public void setUp() {
        library = new Library(loadItems());
        user = User.user("123-1234", "qwerty", "Joey", "joey@friends.com", "9848999999", "customer");
        mockConsoleIO = Mockito.mock(View.class);
    }

    private List<Item> loadItems() {
        Item book1 = new Book(1, "Ignited Minds", "Abdul Kalam", "1995", BOOK);
        Item book2 = new Book(2, "To Kill a Mocking Bird", "Harper Lee", "1955", BOOK);
        Item book3 = new Book(3, "Harry Potter 1", "J K Rowling", "1996", BOOK);
        Item book4 = new Book(4, "Harry Potter 2", "J K Rowling", "1997", BOOK);
        Item book5 = new Book(5, "Harry Potter 3", "J K Rowling", "1999", BOOK);
        Item movie1 = new Movie(1, "Home Alone", "abcdef", "2000", 9.0, MOVIE);
        Item movie2 = new Movie(2, "LOR 1", "asdf", "1998", 8.0, MOVIE);
        Item movie3 = new Movie(3, "LOR 2", "qwerty", "2000", 8.5, MOVIE);
        return new ArrayList<>(Arrays.asList(book1, book2, book3, book4, book5, movie1, movie2, movie3));
    }

    //Book Tests
    @Test
    public void shouldBeAbleToCheckoutAvailableBook() {
        when(mockConsoleIO.read()).thenReturn("1");
        new CheckoutBook(mockConsoleIO, library, user).execute();
        verify(mockConsoleIO).write("Thank you! Enjoy the book");
    }

    @Test
    public void shouldNotBeAbleToCheckoutUnAvailableBook() {
        when(mockConsoleIO.read()).thenReturn("10");
        new CheckoutBook(mockConsoleIO, library, user).execute();
        verify(mockConsoleIO).write("That book is not available.");
    }

    @Test
    public void shouldBeAbleToReturnCheckedOutBook() {
        when(mockConsoleIO.read()).thenReturn("1");
        new CheckoutBook(mockConsoleIO, library, user).execute ();
        when(mockConsoleIO.read()).thenReturn("1");
        new ReturnBook(mockConsoleIO, library, user).execute ();
        verify(mockConsoleIO).write("Thank you for returning the book.");
    }

    @Test
    public void shouldNotBeAbleToReturnAnUnCheckedOutBook() {
        when(mockConsoleIO.read()).thenReturn("1");
        new ReturnBook(mockConsoleIO, library, user).execute ();
        verify(mockConsoleIO).write("That is not a valid book to return.");
    }

    //Movie Tests
    @Test
    public void shouldBeAbleToCheckoutAvailableMovie() {
        when(mockConsoleIO.read()).thenReturn("1");
        new CheckoutMovie(mockConsoleIO, library, user).execute ();
        verify(mockConsoleIO).write("Thank you! Enjoy the Movie");
    }

    @Test
    public void shouldNotBeAbleToCheckoutUnAvailableMovie() {
        when(mockConsoleIO.read()).thenReturn("10");
        new CheckoutMovie(mockConsoleIO, library, user).execute ();
        verify(mockConsoleIO).write("That movie is not available.");
    }

    @Test
    public void shouldBeAbleToReturnACheckedOutMovie() {
        when(mockConsoleIO.read()).thenReturn("1");
        new CheckoutMovie(mockConsoleIO, library, user).execute ();
        when(mockConsoleIO.read()).thenReturn("1");
        new ReturnMovie(mockConsoleIO, library, user).execute ();
        verify(mockConsoleIO).write("Thank you for returning the Movie.");
    }

    @Test
    public void shouldNotBeAbleToReturnAnUnCheckedOutMovie() {
        when(mockConsoleIO.read()).thenReturn("1");
        new ReturnMovie(mockConsoleIO, library, user).execute ();
        verify(mockConsoleIO).write("That is not a valid Movie to return.");
    }
}
